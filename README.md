simple-experiment-pipeline
==========================

This repository is used to centrally manage all Gitlab CI files for simple experiment projects.
These can then be simply imported into the actual projects so that the DRY principle is also observed in the pipelines.

usage
-----

`.gitlab-ci.yml`

```
include:
  { project: 'RalphEng/simple-experiment-pipeline', ref: 'master', file: '/maven/test.yml'}
```

### versions

The `simple-experiment-pipeline` project use the Gitflow workflow.
So `master` branch contains only releases and the `ref: 'master'` refer to the last released version.
If one want to use a specific version of the piplene, then use `ref: 'tags/v0.1.0'` for example.


### note: pipeline badge
Gitlab provides its own badge to indicate the status of the pipeline. 

#### Gitlab Project Badges 
Gitlab is able to display badges in the header of the project overview, between the information about the size of the repository (number of commits, branches...) and the bar showing the distribution of code artefacts.

To make a badge displayed there, one need to add them to the project: `Settings` > `General` > `Badges`
- Name: `Pipeline` 
- Link: `https://gitlab.com/%{project_path}/-/commits/%{default_branch}`
- Badge image Url: `https://gitlab.com/%{project_path}/badges/%{default_branch}/pipeline.svg`


#### Pipleline Badge in `README.md`
Add a badge to the `README.md` to show the status:
```
## Status Master
[![pipeline status](https://gitlab.com/%{project_path}/badges/master/pipeline.svg)](https://gitlab.com/%{project_path}/commits/master)
```

Important: one need to replace `%{project_path}` with the path of the project within git, for example: `RalphEng/experiment-test`.

_(For Markdown, this is just a nested element of an image with a link.)_
  


Notes
-----

This GitLab project does not have its own issue tracker.
It is maintained by [simple-experiment-archetype project]( https://gitlab.com/RalphEng/simple-experiment-archetype) and its [issue tracker](https://gitlab.com/RalphEng/simple-experiment-archetype/-/issues).
